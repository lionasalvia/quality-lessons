import psycopg2.extras

conn_str = "dbname='students' user='students' password='qwerty123' host='localhost'"


def parse_text(filename):
    with open(filename, 'r') as file:
        header = tuple([field.strip() for field in next(file).split('|')])
        next(file)  # пропускаем "----------+--------------------------------------"
        data = []
        for line in file:
            if not (line.isspace() or line.startswith('(')):  # пропускаем "(378 rows)" и пустые строки
                row = tuple([field.strip() for field in line.split('|')])
                data.append(row)
        return header, data


def int_or_none(value):
    try:
        return int(value)
    except ValueError:
        return None


def convert_data(table, types):
    header, data = table
    for i, row in enumerate(data):
        converted_row = []
        for field, value in zip(header, row):
            converted_value = types.get(field, lambda x: x)(value)
            converted_row.append('NULL' if converted_value is None else repr(converted_value))
        data[i] = tuple(converted_row)


def load_table(db_conn, table_name, table):
    header, data = table
    with db_conn.cursor() as cursor:
        for row in set(data):
            sql = f'INSERT INTO "{table_name}" ({", ".join(header)}) VALUES ({", ".join(row)})'
            cursor.execute(sql, row)


def main():
    users = parse_text('data/users.txt')
    lessons = parse_text('data/lessons.txt')
    participants = parse_text('data/participants.txt')
    quality = parse_text('data/quality.txt')

    convert_data(users, {})
    convert_data(lessons, {'event_id': int})
    convert_data(participants, {'event_id': int})
    convert_data(quality, {'tech_quality': int_or_none})

    psycopg2.extras.register_uuid()
    with psycopg2.connect(conn_str) as db_conn:
        load_table(db_conn, 'User', users)
        load_table(db_conn, 'Lesson', lessons)
        load_table(db_conn, 'Participant', participants)
        load_table(db_conn, 'Quality', quality)


if __name__ == "__main__":
    main()
