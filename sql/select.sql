SELECT 
	day, 
	tutor_id, 
	MIN(average_score) AS average_score
FROM 
(SELECT 
	CAST(scheduled_time AS DATE) AS day, 
	user_id AS tutor_id, 
	AVG(tech_quality) AS average_score
FROM 
    "User" JOIN "Participant" ON "User".id = "Participant".user_id
           JOIN "Lesson" ON "Participant".event_id = "Lesson".event_id
		   JOIN "Quality" ON "Lesson".id = "Quality".lesson_id
WHERE
    subject = 'phys' AND tech_quality IS NOT NULL
GROUP BY 
	CAST(scheduled_time AS DATE), 
	user_id, 
	role 
HAVING 
	role = 'tutor'
)
AS ref
GROUP BY 
	day, average_score, tutor_id
ORDER BY 
	day
;