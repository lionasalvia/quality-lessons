DROP TABLE IF EXISTS "Quality";
DROP TABLE IF EXISTS "Participant";
DROP TABLE IF EXISTS "Lesson";
DROP TABLE IF EXISTS "User";

CREATE TABLE "User" (
	id UUID,
	role VARCHAR,
	PRIMARY KEY (id)
);

CREATE TABLE "Lesson" (
	id UUID,
	event_id INTEGER,
	subject VARCHAR,
	scheduled_time TIMESTAMP,
	PRIMARY KEY (id)
);

CREATE TABLE "Participant" (
	user_id UUID,
	event_id INTEGER,
	PRIMARY KEY (user_id, event_id),
	FOREIGN KEY (user_id) REFERENCES "User" (id)
);

CREATE TABLE "Quality" (
	lesson_id UUID,
	tech_quality SMALLINT,
	FOREIGN KEY (lesson_id) REFERENCES "Lesson" (id)
);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO students;